/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef WORLDAPPLICATION_H_
#define WORLDAPPLICATION_H_

#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/Logic.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/event/IListener.h>

/*
 * This example shows how to manage a world and create actors in it.
 *
 * You should see the position and rotation of an actor being updated in the console until
 * it slows down to no movement.
 *
 * See the WorldApplication.cpp file for more.
 */
class WorldApplication: public nox::app::SdlApplication, public nox::logic::event::IListener
{
public:
	WorldApplication();

	bool onInit() override;
	void onDestroy() override;

private:
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	bool initializeResourceCache();
	nox::logic::Logic* initializeLogic();
	nox::logic::physics::Simulation* initializePhysics(nox::logic::Logic* logic);
	nox::logic::world::Manager* initializeWorldManager(nox::logic::Logic* logic);

	bool loadWorldFile(nox::logic::IContext* logicContext, nox::logic::world::Manager* worldManager);

	nox::app::log::Logger log;
	nox::logic::event::ListenerManager eventListener;
};

#endif
