/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "LogicApplication.h"

#include <nox/logic/Logic.h>

#include <json/value.h>
#include <cassert>
#include <random>

LogicApplication::LogicApplication():
	SdlApplication("07-logic", "SuttungDigital")
{
}

void LogicApplication::initializeLogic()
{
	// Create the NOX logic.
	auto logic = std::make_unique<nox::logic::Logic>();

	/*
	 * Add the logic as a process to the application. This how the logic should be managed.
	 * You should not manage it yourself.
	 */
	this->addProcess(std::move(logic));
}

bool LogicApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("LogicApplication");

	/*
	 * That's it. The logic will be managed by the application and be initialized, executed and destroyed properly.
	 * The logic doesn't do much without a world and a physics simulation. Adding these will be explained in a later
	 * example.
	 */
	this->initializeLogic();

	return true;
}
