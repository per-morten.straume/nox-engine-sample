/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "StorageApplication.h"

#include <nox/app/storage/DataStorageBoost.h>

#include <json/value.h>
#include <cassert>
#include <random>
#include <cstdint>

StorageApplication::StorageApplication():
	SdlApplication("06-storage", "SuttungDigital")
{
}

bool StorageApplication::initializeDataStorage()
{
	/*
	 * Create the data storage.
	 * Here the data storage is a DataStorageBoost. This storage reads and writes directly
	 * to the filesystem.
	 */
	auto storage = std::make_unique<nox::app::storage::DataStorageBoost>();

	/*
	 * This gets a directory path we can write our files to.
	 * The directory is per user.
	 */
	const auto storageDirectory = this->getStorageDirectoryPath();

	/*
	 * We try to initialize the storage directory and catch the exception if it failed.
	 */
	try
	{
		// Initialize the storage to the storage directory.
		storage->initialize(storageDirectory);

		/*
		 * No exceptions thrown, so log success and move the storage to the Application base class for management.
		 * The application will automatically destroy the storage when it is shut down. The storage can be accessed
		 * through the nox::app::IContext::getDataStorage() interface.
		 */
		this->log.verbose().format("Data storage initialized to \"%s\"", storageDirectory.c_str());
		this->setDataStorage(std::move(storage));
	}
	catch (const nox::app::storage::DataStorageBoost::IntializationException& exception)
	{
		this->log.error().format("Failed initializing the data storage to \"%s\"", storageDirectory.c_str(), exception.what());

		return false;
	}

	return true;
}

bool StorageApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("StorageApplication");

	/*
	 * We initialize the data storage, and if it fails we return false to abort.
	 */
	if (this->initializeDataStorage() == false)
	{
		this->log.error().raw("Failed to initialize data storage.");
		return false;
	}

	/*
	 * Access the storage that we moved to the application.
	 */
	nox::app::storage::IDataStorage* storage = this->getDataStorage();
	assert(storage != nullptr);

	// This is the file that we want to save to.
	const auto fileName = std::string{"saved_number"};

	if (storage->fileExists(fileName) == true)
	{
		/*
		 * If the file exists, we open it for reading and read in a single number.
		 */

		std::unique_ptr<std::istream> inputStream = storage->openReadableFile(fileName);
		std::uint32_t number = 0;

		*inputStream >> number;

		this->log.info().format("Read %u from storage.", number);
	}
	else
	{
		this->log.info().raw("Found no stored number.");
	}

	/*
	 * Get a random number between 0 and 1024.
	 */
	std::random_device randomDevice;
	auto randomEngine = std::minstd_rand0{randomDevice()};
	auto numberDistribution = std::uniform_int_distribution<std::uint32_t>{0, 1024};
	const auto number = numberDistribution(randomEngine);

	/*
	 * Open the file for writing so that we can write the new number.
	 * The storage automatically creates files that doesn't exist, so it's safe
	 * to assume that the outputStream is valid.
	 */
	std::unique_ptr<std::ostream> outputStream = storage->openWritableFile(fileName);
	assert(outputStream != nullptr);

	// Write the number to the storage.
	this->log.info().format("Writing new number to storage: %u", number);
	*outputStream << number;

	return true;
}
