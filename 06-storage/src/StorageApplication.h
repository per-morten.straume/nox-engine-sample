/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef STORAGEAPPLICATION_H_
#define STORAGEAPPLICATION_H_

#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>

/*
 * This example shows how to set up a data storage and how
 * to write and read from it.
 *
 * The purpose of the data storage is to persistently store data, such as configuration and save files,
 * so that it can be loaded at a later time.
 *
 * On initialization you should see "Data storage initialized to "y"" in the console, where y is a directory path.
 * The directory path shown is where all files saved to the data storage will be.
 *
 * During the first run you should see "Found no stored number." because there was no saved file. It will the write a new random
 * number to the file and output the message "Writing new number to storage: x" where x is the number written. The next time you
 * run it you should see "Read x from storage.", where x is the same number as last time. It will then write yet another number.
 *
 * See the StorageApplication.cpp file for more.
 */
class StorageApplication: public nox::app::SdlApplication
{
public:
	StorageApplication();

	bool onInit() override;

private:
	bool initializeDataStorage();

	nox::app::log::Logger log;
};

#endif
