/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ExampleApplication.h"

/*
 * This time we set the application name and organization name directly in
 * our example application constructor.
 */
ExampleApplication::ExampleApplication():
	SdlApplication("02-extending-application", "SuttungDigital")
{
}

/*
 * This function is called when nox::app::Application has finished its initialization
 * and lets subclasses do their initialization.
 *
 * If we return false from this function, the application initialization will fail gracefully,
 * if not it will succeed and execute properly.
 */
bool ExampleApplication::onInit()
{
	/*
	 * Execute the SdlApplication's initialization. This is our responsibility since
	 * we override this function from SdlApplication. Without this, SDL would not
	 * be properly initialized. As a general rule, you should always call the base class
	 * function when overriding a function, if it is not pure virtual or the documentation
	 * states otherwise.
	 */
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	/*
	 * To be able to log information we have to create our own logger through the application.
	 * We give the logger a name, so that it can easily be identified from the console output.
	 */
	this->log = this->createLogger();
	this->log.setName("ExampleApplication");

	/*
	 * This logs an informative message that will appear in the console. There are several different
	 * log levels (where info is one of them). This can be filtered (in a later version) so that only
	 * certain log levels will be outputted (normally you wouldn't want debug log output).
	 *
	 * In the console you will see this message as "[info][ExampleApplication] I've successfully initialized my very own application!"
	 */
	this->log.info().raw("I've successfully initialized my very own application!");

	/*
	 * As the initialization was successful we return true to allow the application to execute.
	 */
	return true;
}
